**RunnersApp**

Esta aplicacion esta desarrollada utilizando el framework de flutter, tambien se hace uso de los servicios de **openrouteservice** para la integracion de Sistemas de Informacion Geograficas. Esto es utilizado dentro de la aplicacion para la creacion de rutas, mapas, entre otras cosas.

Para mas Informacion sobre la instalacion, utilizacion y mantenimiento del aplicativo consultar el manual tecnico que se encuentra dentro del repositorio como **"Manual Tecnico 'Runners App'"**.

BLoC implementado en la rama main para el manejo del estado de la aplicacion. Editado el 6 de Diciembre de 2020.

**TODO**

- [ ] Implementar funcionalidad para guardar informacion sobre la ubicacion del dispositivo en tiempo real.
- [ ] Implementar pantalla para el perfil del usuario.

**Capturas de Pantalla**
Para ver las capturas de pantalla por favor visitar el manual de usuario que se encuentra dentro del repositorio como **"Manual de usuario 'Runners App'"**.
