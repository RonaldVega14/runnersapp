import 'package:flutter/material.dart';
import 'package:runners_app/bloc/auth_bloc.dart';
import 'package:runners_app/controllers/home_controller.dart';
import 'package:runners_app/events/auth_events.dart';
import 'package:runners_app/pages/splash_page.dart';
import 'package:runners_app/repositories/user_repository.dart';
import 'package:runners_app/states/auth_states.dart';
import 'pages/home_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  AuthBloc _authBloc;

  @override
  void initState() {
    _authBloc = AuthBloc(
      AuthenticationUninitialized(),
      userRepository: UserRepository(),
    );
    _authBloc.add(AppStarted());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => _authBloc,
      child: WillPopScope(
        onWillPop: () => Future.value(false),
        child: MaterialApp(
            debugShowCheckedModeBanner: false,
            home: BlocListener<AuthBloc, AuthState>(
              cubit: _authBloc,
              listener: (BuildContext context, state) {},
              child: BlocBuilder(
                  cubit: _authBloc,
                  //ignore: missing_return
                  builder: (BuildContext context, AuthState state) {
                    return AnimatedSwitcher(
                      duration: Duration(milliseconds: 250),
                      switchOutCurve: Threshold(0.05),
                      transitionBuilder:
                          (Widget child, Animation<double> animation) {
                        return SlideTransition(
                          position: Tween<Offset>(
                            begin: const Offset(0.25, 0),
                            end: Offset.zero,
                          ).animate(animation),
                          child: child,
                        );
                      },
                      child: _buildPage(context, state),
                    );
                  }),
            )),
      ),
    );
  }

  _buildPage(BuildContext context, AuthState state) {
    if (state is AuthenticationUninitialized) {
      return SplashPage();
    } else if (state is AuthenticationAuthenticated) {
      return HomeController(
        user: state.user,
      );
    } else if (state is AuthenticationUnauthenticated) {
      return Scaffold(
        resizeToAvoidBottomInset: true,
        resizeToAvoidBottomPadding: false,
        body: HomeScreen(
          authBloc: _authBloc,
        ),
      );
    } else if (state is AuthenticationLoading) {
      return SplashPage();
    }
  }
}
