import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:runners_app/bloc/home_bloc.dart';
import 'package:runners_app/events/home_events.dart';
import 'package:runners_app/models/group_model.dart';
import 'package:runners_app/repositories/network_helper.dart';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:runners_app/styles.dart';

class AddRoutePage extends StatefulWidget {
  final String title;
  final Group group;
  final HomeBloc homeBloc;
  final bool backToHome;

  const AddRoutePage(
      {Key key,
      @required this.title,
      this.group,
      @required this.homeBloc,
      this.backToHome = false})
      : super(key: key);
  @override
  _AddRoutePageState createState() => _AddRoutePageState();
}

class _AddRoutePageState extends State<AddRoutePage> {
  GoogleMapController mapController;
  TextEditingController routeNameController = TextEditingController();
  // For holding Co-ordinates as LatLng
  final List<LatLng> polyPoints = [];

//For holding instance of Polyline
  final Set<Polyline> polyLines = {};
// For holding instance of Marker
  final Set<Marker> markers = {};
  var data;

  // Dummy Start and Destination Points
  double defaultStartLat = 13.668436;
  double defaultStartLng = -89.261156;
  double defaultEndLat = 13.670015;
  double defaultEndLng = -89.255899;

  double savedStartLat = 13.668436;
  double savedStartLng = -89.261156;
  double savedEndLat = 13.670015;
  double savedEndLng = -89.255899;

  double distance = 0, duration = 0;

  final NetworkHelper network = NetworkHelper();

  HomeBloc get homeBloc => widget.homeBloc;

  bool isWalking = true;

  Group get group => widget.group;

  @override
  void initState() {
    super.initState();
    getJsonData(defaultStartLat, defaultStartLng, defaultEndLat, defaultEndLng);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
              ),
              onPressed: () => widget.backToHome
                  ? Navigator.pop(context)
                  : homeBloc.add(ToGroupDetailsPage(group: group))),
          iconTheme: IconThemeData(color: Styles.mainColor),
          title: Text(widget.title,
              style: TextStyle(color: Styles.mainColor, fontSize: 17.0)),
          backgroundColor: Colors.white,
        ),
        body: Stack(
          children: [
            GoogleMap(
              myLocationButtonEnabled: true,
              myLocationEnabled: true,
              onTap: _handleTap,
              onMapCreated: _onMapCreated,
              initialCameraPosition: CameraPosition(
                target: LatLng(defaultStartLat, defaultStartLng),
                zoom: 15,
              ),
              markers: markers,
              polylines: polyLines,
            ),
            Positioned(
                bottom: 40,
                left: 20.0,
                child: InkWell(
                  onTap: _deleteRoute,
                  child: optionContainer(
                      context, Icons.delete_forever_outlined, 'Eliminar ruta'),
                )),
            group != null
                ? Positioned(
                    bottom: 100.0,
                    left: 20.0,
                    child: InkWell(
                      onTap: () => createAddRouteDialog(context),
                      child: optionContainer(
                          context, Icons.save_outlined, 'Guardar ruta'),
                    ))
                : SizedBox(),
            group != null
                ? Positioned(
                    bottom: 160.0,
                    left: 20.0,
                    child: InkWell(
                      splashColor: Colors.grey,
                      onTap: () => _changeRoute(markers),
                      child: optionContainer(
                          context,
                          isWalking
                              ? Icons.directions_bike_outlined
                              : Icons.directions_walk_outlined,
                          'Cambiar modo de ruta'),
                    ))
                : Positioned(
                    bottom: 100.0,
                    left: 20.0,
                    child: InkWell(
                      splashColor: Colors.grey,
                      onTap: () => _changeRoute(markers),
                      child: optionContainer(
                          context,
                          isWalking
                              ? Icons.directions_bike_outlined
                              : Icons.directions_walk_outlined,
                          'Cambiar modo de ruta'),
                    )),
            Positioned(
                bottom: 15.0,
                left: MediaQuery.of(context).size.width * 0.25,
                child: InkWell(
                  onTap: () => _changeRoute(markers),
                  child: Container(
                      height: MediaQuery.of(context).size.height * 0.12,
                      width: MediaQuery.of(context).size.width * 0.51,
                      padding: EdgeInsets.all(12.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12.0),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            offset: Offset(0.0, 1.0), //(x,y)
                            blurRadius: 6.0,
                          ),
                        ],
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Flexible(
                            flex: 2,
                            fit: FlexFit.loose,
                            child: Text(
                              'Distance: ' + distance.toString() + ' km',
                              style: TextStyle(fontSize: 16.0),
                            ),
                          ),
                          Flexible(
                            flex: 2,
                            fit: FlexFit.loose,
                            child: Text(
                                'Duration: ' + duration.toString() + ' mins',
                                style: TextStyle(fontSize: 16.0)),
                          ),
                        ],
                      )),
                ))
          ],
        ));
  }

  Widget optionContainer(BuildContext context, IconData icon, String title) {
    return Tooltip(
      message: title,
      child: Container(
          height: 45,
          width: 45,
          padding: EdgeInsets.all(8.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12.0),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                offset: Offset(0.0, 1.0), //(x,y)
                blurRadius: 6.0,
              ),
            ],
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Icon(
                icon,
                size: 20.0,
              ),
            ],
          )),
    );
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    setMarkers();
  }

  void getJsonData(
      double startLat, double startLng, double endLat, double endLng) async {
    // Create an instance of Class NetworkHelper which uses http package
    // for requesting data to the server and receiving response as JSON format

    try {
      data = await network.getData(
          startLat: startLat,
          startLng: startLng,
          endLat: endLat,
          endLng: endLng,
          pathParam: 'foot-walking');

      // We can reach to our desired JSON data manually as following
      LineString ls =
          LineString(data['features'][0]['geometry']['coordinates']);

      distance = data['features'][0]['properties']['summary']['distance'];
      duration = data['features'][0]['properties']['summary']['duration'];

      distance = double.parse((distance / 1000).toStringAsFixed(2));
      duration = double.parse((duration / 60).toStringAsFixed(2));

      for (int i = 0; i < ls.lineString.length; i++) {
        polyPoints.add(LatLng(ls.lineString[i][1], ls.lineString[i][0]));
      }
      setPolyLines();
    } catch (e) {
      print(e);
    }
  }

  void getRoute(Set<Marker> points, {String pathParam}) async {
    // Create an instance of Class NetworkHelper which uses http package
    // for requesting data to the server and receiving response as JSON format
    double startLat, startLng, endLat, endLng;
    Marker start = points.elementAt(0);
    Marker end = points.elementAt(1);

    startLat = start.position.latitude;
    startLng = start.position.longitude;

    endLat = end.position.latitude;
    endLng = end.position.longitude;

    try {
      // getData() returns a json Decoded data
      data = await network.getData(
          startLat: startLat,
          startLng: startLng,
          endLat: endLat,
          endLng: endLng,
          pathParam: pathParam);

      if (data != null) {
        // We can reach to our desired JSON data manually as following
        LineString ls =
            LineString(data['features'][0]['geometry']['coordinates']);

        savedStartLat = startLat;
        savedStartLng = startLng;
        savedEndLat = endLat;
        savedEndLng = endLng;

        distance = data['features'][0]['properties']['summary']['distance'];
        duration = data['features'][0]['properties']['summary']['duration'];

        distance = double.parse((distance / 1000).toStringAsFixed(2));
        duration = double.parse((duration / 60).toStringAsFixed(2));

        polyPoints.clear();

        for (int i = 0; i < ls.lineString.length; i++) {
          polyPoints.add(LatLng(ls.lineString[i][1], ls.lineString[i][0]));
        }
        setPolyLines();
      } else {
        showToast(
            'Error al calcular la ruta, intente nuevamente', Colors.redAccent);
      }
      Navigator.pop(context);
    } catch (e) {
      print(e);
    }
  }

  setPolyLines() {
    setState(() {
      Polyline polyline = Polyline(
        polylineId: PolylineId("polyline"),
        color: Colors.lightBlue,
        points: polyPoints,
      );
      polyLines.add(polyline);
    });
  }

  setMarkers() {
    setState(() {
      markers.add(
        Marker(
          markerId: MarkerId("Origen"),
          position: LatLng(defaultStartLat, defaultStartLng),
          infoWindow: InfoWindow(
            title: "Origen",
            snippet: "Punto de partida",
          ),
        ),
      );
      markers.add(Marker(
        markerId: MarkerId("Destino"),
        position: LatLng(defaultEndLat, defaultEndLng),
        infoWindow: InfoWindow(
          title: "Destino",
          snippet: "Punto de destino",
        ),
      ));
    });
  }

  void _handleTap(LatLng point) {
    if (markers.length < 1) {
      setState(() {
        markers.add(Marker(
          markerId: MarkerId("Origen"),
          position: LatLng(point.latitude, point.longitude),
          infoWindow: InfoWindow(
            title: "Origen",
            snippet: "Punto de partida",
          ),
        ));
      });
    } else if (markers.length < 2) {
      setState(() {
        markers.add(Marker(
          markerId: MarkerId("Destino"),
          position: LatLng(point.latitude, point.longitude),
          infoWindow: InfoWindow(
            title: "Destino",
            snippet: "Punto de destino",
          ),
        ));
      });
      createLoadingRouteDialog(context, 'Calculando Ruta...');
      getRoute(markers, pathParam: isWalking ? 'foot-walking' : 'cycling-road');
    } else {
      showToast('Debe eliminar la ruta antes de calcular una nueva',
          Colors.redAccent);
    }
  }

  void showToast(String msg, Color color) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        timeInSecForIosWeb: 3,
        backgroundColor: color,
        textColor: Colors.white,
        fontSize: 15.0);
  }

  static createLoadingRouteDialog(BuildContext context, String msg) async {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) => Center(
                child: Container(
              margin: EdgeInsets.symmetric(horizontal: 40.0),
              width: MediaQuery.of(context).size.width * 0.8,
              height: 220,
              child: Material(
                color: Colors.white.withOpacity(0.9),
                elevation: 5.0,
                shadowColor: Colors.black,
                borderRadius: BorderRadius.circular(14.0),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 15.0, vertical: 15.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Flexible(
                          fit: FlexFit.tight,
                          flex: 4,
                          child: Text(
                            msg.toUpperCase(),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 18),
                          )),
                      Flexible(
                          fit: FlexFit.loose,
                          flex: 6,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 15.0),
                            child: CircularProgressIndicator(
                              strokeWidth: 2,
                            ),
                          )),
                    ],
                  ),
                ),
              ),
            )));
  }

  createAddRouteDialog(BuildContext context) async {
    showDialog(
        context: context,
        builder: (context) => Center(
                child: Container(
              margin: EdgeInsets.symmetric(horizontal: 40.0),
              width: MediaQuery.of(context).size.width * 0.8,
              height: 240,
              child: Material(
                color: Colors.white.withOpacity(0.9),
                elevation: 5.0,
                shadowColor: Colors.black,
                borderRadius: BorderRadius.circular(14.0),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 15.0, vertical: 15.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Flexible(
                          fit: FlexFit.loose,
                          flex: 7,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(
                                'Agregar ruta a: '.toUpperCase(),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 18),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Text(
                                  group.name.toUpperCase(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              SizedBox(height: 8.0),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 12.0, horizontal: 14.0),
                                child: routeName(),
                              )
                            ],
                          )),
                      Flexible(
                          fit: FlexFit.loose,
                          flex: 4,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                child: customButton(
                                    labelText: 'Agregar',
                                    function: () => _saveRoute(
                                        group.id, routeNameController.text)),
                                flex: 5,
                                fit: FlexFit.tight,
                              ),
                              Flexible(
                                child: customButton(
                                    labelText: 'Cancelar',
                                    isCancel: true,
                                    function: () => {
                                          routeNameController.clear(),
                                          Navigator.pop(context)
                                        }),
                                flex: 5,
                                fit: FlexFit.tight,
                              )
                            ],
                          )),
                    ],
                  ),
                ),
              ),
            )));
  }

  Widget customButton(
          {String labelText = 'Aceptar',
          Function function,
          bool isCancel = false}) =>
      InkWell(
        onTap: function,
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 5.0),
          width: MediaQuery.of(context).size.width * 0.25,
          height: 40,
          padding: EdgeInsets.symmetric(horizontal: 12),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
              color: isCancel ? Colors.red : Colors.blueAccent),
          child: Center(
            child: FittedBox(
              child: Text(
                labelText.toUpperCase(),
                style: TextStyle(
                    fontSize: 14,
                    color: Colors.white,
                    fontWeight: FontWeight.w400),
              ),
            ),
          ),
        ),
      );

  Widget routeName() => TextFormField(
        validator: (val) => !val.contains('@') ? 'Usuario Incorrecto' : null,
        controller: routeNameController,
        style: TextStyle(
            fontFamily: 'Montserrat', fontSize: 15.0, color: Colors.black),
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.person),
            labelText: 'Nombre de la ruta',
            contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            border: OutlineInputBorder(
                borderSide: BorderSide(
                    color: Colors.blueAccent,
                    width: 1.0,
                    style: BorderStyle.solid),
                borderRadius: BorderRadius.circular(25.0))),
      );

  void _deleteRoute() {
    markers.clear();
    polyLines.clear();
    polyPoints.clear();
    distance = 0.0;
    duration = 0.0;
    setState(() {});
  }

  void _changeRoute(Set<Marker> markers) async {
    if (markers.isNotEmpty && markers != null) {
      isWalking = !isWalking;
      createLoadingRouteDialog(context, 'Calculando Ruta...');
      getRoute(markers, pathParam: isWalking ? 'foot-walking' : 'cycling-road');
      isWalking
          ? showToast('Ruta calculada para viajar caminando', Colors.blueAccent)
          : showToast(
              'Ruta calculada para viajar en bicicleta', Colors.blueAccent);
      setState(() {});
    } else {
      showToast('Seleccione una ruta', Colors.redAccent);
    }
  }

  void _saveRoute(String groupId, String name) async {
    Navigator.pop(context);
    createLoadingRouteDialog(context, 'Guardando Ruta...');
    homeBloc.add(AddRouteToGroup(
        name: name,
        group: group,
        startLat: savedStartLat.toString(),
        startLng: savedStartLng.toString(),
        endLat: savedEndLat.toString(),
        endLng: savedEndLng.toString()));
    Navigator.pop(context);
  }
}

class LineString {
  LineString(this.lineString);
  List<dynamic> lineString;
}
