import 'package:flutter/material.dart';
import 'package:runners_app/bloc/auth_bloc.dart';
import 'package:runners_app/events/auth_events.dart';
import 'package:runners_app/repositories/user_repository.dart';
import 'package:runners_app/states/auth_states.dart';

class SignUpScreen extends StatefulWidget {
  final AuthBloc authBloc;

  const SignUpScreen({Key key, @required this.authBloc}) : super(key: key);
  @override
  _SignUpScreenState createState() => new _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen>
    with TickerProviderStateMixin {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();
  AuthBloc get bloc => widget.authBloc;

  @override
  void initState() {
    super.initState();
  }

  TextStyle style =
      TextStyle(fontFamily: 'Montserrat', fontSize: 14.0, color: Colors.black);
  TextEditingController nombreController = new TextEditingController();
  TextEditingController apellidoController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController userController = new TextEditingController();
  TextEditingController passController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
//Nombre
    final nombreField = TextFormField(
      validator: (val) => val.isEmpty ? 'Campo necesario' : null,
      controller: nombreController,
      style: TextStyle(
          fontFamily: 'Montserrat', fontSize: 14.0, color: Colors.black),
      decoration: InputDecoration(
          prefixIcon: Icon(Icons.person),
          labelText: 'Nombre',
          hintText: 'Nombre, según DUI',
          contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(25.0))),
    );
//Apellido
    final apellidoField = TextFormField(
      validator: (val) => !(val.length < 5) ? 'Contraseña Invalida' : null,
      controller: apellidoController,
      style: TextStyle(
          fontFamily: 'Montserrat', fontSize: 14.0, color: Colors.black),
      decoration: InputDecoration(
          prefixIcon: Icon(Icons.person),
          labelText: 'Apellidos',
          hintText: 'Apellidos, según DUI',
          contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(25.0))),
    );
//Email
    final emailField = TextFormField(
      validator: (val) => !val.contains('@') ? 'Usuario Incorrecto' : null,
      controller: emailController,
      style: TextStyle(
          fontFamily: 'Montserrat', fontSize: 15.0, color: Colors.black),
      decoration: InputDecoration(
          prefixIcon: Icon(Icons.mail),
          labelText: 'Correo Electrónico',
          hintText: 'Email',
          contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
          border: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Colors.blueAccent,
                  width: 1.0,
                  style: BorderStyle.solid),
              borderRadius: BorderRadius.circular(25.0))),
    );
//Cuenta CEDEVAL o Cuenta...
    final userField = Container(
      child: TextFormField(
        validator: (val) => !val.contains('') ? 'Usuario ya existente' : null,
        controller: userController,
        style: TextStyle(
            fontFamily: 'Montserrat', fontSize: 15.0, color: Colors.black),
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.person),
            labelText: 'Usuario',
            hintText: 'Usuario',
            contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            border: OutlineInputBorder(
                borderSide: BorderSide(
                    color: Colors.blueAccent,
                    width: 1.0,
                    style: BorderStyle.solid),
                borderRadius: BorderRadius.circular(25.0))),
      ),
    );
    //Password
    final passwordField = TextFormField(
      validator: (val) => !val.contains('@') ? 'Usuario Incorrecto' : null,
      controller: passController,
      obscureText: true,
      style: TextStyle(
          fontFamily: 'Montserrat', fontSize: 15.0, color: Colors.black),
      decoration: InputDecoration(
          prefixIcon: Icon(Icons.vpn_key),
          labelText: 'Password',
          hintText: 'Password',
          contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
          border: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Colors.blueAccent,
                  width: 1.0,
                  style: BorderStyle.solid),
              borderRadius: BorderRadius.circular(25.0))),
    );

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          color: Colors.white,
          image: DecorationImage(
            colorFilter: new ColorFilter.mode(
                Colors.black.withOpacity(0.05), BlendMode.dstATop),
            image: AssetImage('assets/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: new Form(
              key: formKey,
              child: SingleChildScrollView(
                reverse: false,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.fromLTRB(80.0, 20.0, 80.0, 5.0),
                      child: Center(
                        child: SizedBox(
                          height: 70.0,
                          width: 190,
                          child: Image.asset(
                            "assets/runner_logo.png",
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding:
                              const EdgeInsets.only(top: 5.0, bottom: 10.0),
                          child: Text(
                            "Registrate como atleta",
                            style: style.copyWith(
                              fontWeight: FontWeight.bold,
                              color: Colors.blueAccent,
                              fontSize: 18.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(padding: const EdgeInsets.only(bottom: 10.0)),
                    Container(
                        width: MediaQuery.of(context).size.width,
                        margin: const EdgeInsets.only(
                            left: 35.0, right: 35.0, top: 5.0),
                        alignment: Alignment.center,
                        padding: const EdgeInsets.only(left: 0.0, right: 10.0),
                        child: nombreField),
                    Padding(padding: const EdgeInsets.only(top: 7.0)),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.center,
                      child: Container(
                          width: MediaQuery.of(context).size.width,
                          margin: const EdgeInsets.only(
                              left: 35.0, right: 35.0, top: 5.0),
                          alignment: Alignment.center,
                          padding:
                              const EdgeInsets.only(left: 0.0, right: 10.0),
                          child: apellidoField),
                    ),
                    Padding(padding: const EdgeInsets.only(top: 7.0)),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.center,
                      child: Container(
                          width: MediaQuery.of(context).size.width,
                          margin: const EdgeInsets.only(
                              left: 35.0, right: 35.0, top: 5.0),
                          alignment: Alignment.center,
                          padding:
                              const EdgeInsets.only(left: 0.0, right: 10.0),
                          child: emailField),
                    ),
                    Padding(padding: const EdgeInsets.only(top: 7.0)),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.center,
                      child: Container(
                          width: MediaQuery.of(context).size.width,
                          margin: const EdgeInsets.only(
                              left: 35.0, right: 35.0, top: 5.0),
                          alignment: Alignment.center,
                          padding:
                              const EdgeInsets.only(left: 0.0, right: 10.0),
                          child: passwordField),
                    ),
                    Padding(padding: const EdgeInsets.only(top: 7.0)),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.center,
                      child: Container(
                          width: MediaQuery.of(context).size.width,
                          margin: const EdgeInsets.only(
                              left: 35.0, right: 35.0, top: 5.0),
                          alignment: Alignment.center,
                          padding:
                              const EdgeInsets.only(left: 0.0, right: 10.0),
                          child: userField),
                    ),
                    Divider(
                      height: 20.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right: 20.0),
                          child: FlatButton(
                            child: Text(
                              "¿Ya tiene un usuario?",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.blueAccent,
                                fontSize: 15.0,
                              ),
                              textAlign: TextAlign.end,
                            ),
                            onPressed: () => {},
                          ),
                        ),
                      ],
                    ),
                    Padding(padding: const EdgeInsets.only(bottom: 5.0)),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.5,
                      margin: const EdgeInsets.only(
                          left: 30.0, right: 30.0, bottom: 15.0),
                      alignment: Alignment.center,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: FlatButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12.0),
                              ),
                              color: Colors.blueAccent,
                              onPressed: () => {
                                registerMethod(),
                              },
                              child: Container(
                                padding: const EdgeInsets.symmetric(
                                  vertical: 14.0,
                                  horizontal: 10.0,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Expanded(
                                      child: Text("REGISTRARSE",
                                          textAlign: TextAlign.center,
                                          style: style.copyWith(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )),
        ),
      ),
    );
  }

  registerMethod() async {
    bloc.add(RegisterUser(
        apellidos: apellidoController.text,
        contra: passController.text,
        correo: emailController.text,
        nombres: nombreController.text,
        usuario: userController.text));
  }
}
