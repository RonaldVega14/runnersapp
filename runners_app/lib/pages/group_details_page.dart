import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:runners_app/bloc/home_bloc.dart';
import 'package:runners_app/events/home_events.dart';
import 'package:runners_app/models/group_model.dart';
import 'package:runners_app/models/route_model.dart';
import 'package:runners_app/pages/add_route_page.dart';
import 'package:runners_app/repositories/group_repository.dart';
import 'package:runners_app/styles.dart';

class GroupDetailsPage extends StatefulWidget {
  final Group group;
  final HomeBloc homeBloc;

  const GroupDetailsPage(
      {Key key, @required this.group, @required this.homeBloc})
      : super(key: key);
  @override
  _GroupDetailsPageState createState() => _GroupDetailsPageState();
}

class _GroupDetailsPageState extends State<GroupDetailsPage> {
  Group get group => widget.group;
  List<MapRoute> groupRoutes = [];
  GroupRepository groupRepository = GroupRepository();
  bool hasRoutes = false;
  HomeBloc get homeBloc => widget.homeBloc;

  @override
  void initState() {
    if (group.routes.length > 0) {
      groupRoutes = group.routes;
      hasRoutes = true;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => homeBloc.add(ToMyGroupsPage()),
            ),
            iconTheme: IconThemeData(color: Styles.mainColor, size: 28.0),
            backgroundColor: Colors.white,
            expandedHeight: 200.0,
            floating: false,
            pinned: true,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              title: Container(
                padding: EdgeInsets.symmetric(vertical: 2.0, horizontal: 5.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6.0),
                  color: Colors.white60,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.white24,
                      offset: Offset(0.0, 1.0), //(x,y)
                      blurRadius: 2.0,
                    ),
                  ],
                ),
                child: Text(group.name,
                    style: TextStyle(fontSize: 20.0, color: Styles.mainColor)),
              ),
              background: Image.asset(
                'assets/groupDetailsBg.jpg',
                fit: BoxFit.cover,
                colorBlendMode: BlendMode.dstATop,
                color: Colors.black,
              ),
            ),
          ),
          SliverFillRemaining(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: ListView(
                physics: NeverScrollableScrollPhysics(),
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text('Descripcion',
                        style: TextStyle(
                            fontSize: 20.0,
                            color: Colors.black,
                            fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 10.0, left: 8.0, bottom: 15.0),
                    child: Text(group.description,
                        style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.black,
                        )),
                  ),
                  Divider(
                    height: 2.0,
                    thickness: 2.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text('Codigo de Acceso: ',
                              style: TextStyle(
                                  fontSize: 18.0,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold)),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(group.accessCode,
                              style: TextStyle(
                                fontSize: 16.0,
                                color: Colors.black,
                              )),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text('Numero de miembros: ',
                              style: TextStyle(
                                  fontSize: 18.0,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold)),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 8.0,
                          ),
                          child: Text(group.members.length.toString() ?? '0',
                              style: TextStyle(
                                fontSize: 16.0,
                                color: Colors.black,
                              )),
                        ),
                      ],
                    ),
                  ),
                  Divider(
                    height: 2.0,
                    thickness: 2.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 15.0, left: 8.0, bottom: 5.0),
                    child: Text('Rutas',
                        style: TextStyle(
                            fontSize: 20.0,
                            color: Colors.black,
                            fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: hasRoutes
                        ? ListView.builder(
                            shrinkWrap: true,
                            itemBuilder: (context, index) =>
                                _routeContaienr(group.routes[index]),
                            itemCount: group.routes.length,
                          )
                        : Padding(
                            padding: const EdgeInsets.only(top: 3.0),
                            child:
                                Text('Este grupo no tiene ninguna ruta creada',
                                    style: TextStyle(
                                      fontSize: 16.0,
                                      color: Colors.black,
                                    )),
                          ),
                  ),
                  SizedBox(
                    height: 25.0,
                  ),
                  customButton(labelText: 'Crear Ruta'),
                  SizedBox(
                    height: 25.0,
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget customButton({String labelText = 'Aceptar'}) => GestureDetector(
        onTap: _openMapScreen,
        child: Center(
          child: Container(
            width: MediaQuery.of(context).size.width * 0.45,
            height: 40,
            padding: EdgeInsets.symmetric(horizontal: 12),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                color: Styles.mainColor),
            child: Center(
              child: FittedBox(
                child: Text(
                  labelText.toUpperCase(),
                  style: TextStyle(
                      fontSize: 14,
                      color: Colors.white,
                      fontWeight: FontWeight.w400),
                ),
              ),
            ),
          ),
        ),
      );

  Widget _routeContaienr(MapRoute route) => Padding(
      padding: const EdgeInsets.only(top: 5.0, left: 8.0, bottom: 5.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(left: 0.0),
                  child: Text('Nombre de ruta: ',
                      style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.black,
                          fontWeight: FontWeight.bold)),
                ),
                flex: 5,
                fit: FlexFit.tight,
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Text(route.name,
                      style: TextStyle(
                        fontSize: 15.0,
                        color: Colors.black,
                      )),
                ),
                flex: 6,
                fit: FlexFit.loose,
              )
            ],
          ),
          SizedBox(
            height: 3.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(left: 0.0),
                  child: Text('Fecha creada: ',
                      style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.black,
                          fontWeight: FontWeight.bold)),
                ),
                flex: 5,
                fit: FlexFit.tight,
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Text(route.createdAt,
                      style: TextStyle(
                        fontSize: 15.0,
                        color: Colors.black,
                      )),
                ),
                flex: 6,
                fit: FlexFit.loose,
              )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Divider(
            height: 2.0,
            thickness: 2.0,
          ),
        ],
      ));

  void _openMapScreen() async {
    homeBloc.add(
        ToCreateRoutePage(group: group, title: 'Agregar ruta a ${group.name}'));
  }
}
