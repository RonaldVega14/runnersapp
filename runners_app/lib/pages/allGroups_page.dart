import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:runners_app/bloc/home_bloc.dart';
import 'package:runners_app/events/home_events.dart';
import 'package:runners_app/models/group_model.dart';
import 'package:runners_app/styles.dart';

class AllGroupsPage extends StatefulWidget {
  final HomeBloc homeBloc;
  final List<Group> groups;

  const AllGroupsPage({Key key, @required this.homeBloc, @required this.groups})
      : super(key: key);
  @override
  _AllGroupsPageState createState() => _AllGroupsPageState();
}

class _AllGroupsPageState extends State<AllGroupsPage> {
  List<Group> get allGroups => widget.groups;
  List<Group> get myGroups => widget.homeBloc.user.groups;
  HomeBloc get homeBloc => widget.homeBloc;
  bool loadingGroup = false;
  TextEditingController accessCodeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          centerTitle: true,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back_outlined,
                color: Styles.mainColor,
              ),
              onPressed: () => homeBloc.add(ToHomePage())),
        ),
        body: Container(
          height: MediaQuery.of(context).size.height,
          child: ListView(
            children: [
              Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 15.0),
                  child: Text(
                    'Selecciona un grupo para unirte',
                    style: TextStyle(
                      color: Styles.darkerColor,
                      fontSize: 26,
                    ),
                    textAlign: TextAlign.center,
                  )),
              ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (context, index) =>
                    groupContainer(allGroups[index]),
                itemCount: allGroups.length,
              ),
              SizedBox(
                height: 30.0,
              )
            ],
          ),
        ));
  }

  void joinGroup(String groupId, String accessCode) async {
    if (accessCodeController.text.isNotEmpty &&
        accessCodeController.text != null) {
      accessCodeController.clear();
      Navigator.pop(context);
      homeBloc.add(JoinGroup(
          accessCode: accessCode, groupID: groupId, groups: allGroups));
    } else {
      Fluttertoast.showToast(
          msg: 'Ingrese el codigo de acceso para unirse al grupo',
          backgroundColor: Colors.redAccent);
      loadingGroup = !loadingGroup;
    }
  }

  Widget groupContainer(Group group) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
        padding: EdgeInsets.symmetric(vertical: 3.0, horizontal: 15.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12.0),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              offset: Offset(0.0, 1.0), //(x,y)
              blurRadius: 6.0,
            ),
          ],
        ),
        height: 80,
        width: MediaQuery.of(context).size.width,
        child: Row(
          children: [
            Flexible(
              flex: 8,
              fit: FlexFit.tight,
              child: Column(
                children: [
                  Flexible(
                      flex: 2,
                      fit: FlexFit.tight,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'Nombre del grupo: ',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16.0),
                          ),
                          Text(
                            group.name,
                            style: TextStyle(fontSize: 16.0),
                          )
                        ],
                      )),
                  Flexible(
                      flex: 2,
                      fit: FlexFit.tight,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'Numero de miembros: ',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16.0),
                          ),
                          Text(
                            group.members != null
                                ? group.members.length.toString()
                                : '0',
                            style: TextStyle(fontSize: 16.0),
                          )
                        ],
                      ))
                ],
              ),
            ),
            Flexible(
              child: GestureDetector(
                onTap: () => _verifyParticipants(group)
                    ? null
                    : createAddGroupDialog(context, group),
                child: _verifyParticipants(group)
                    ? Icon(
                        Icons.done,
                        size: 40.0,
                        color: Colors.green,
                      )
                    : Icon(
                        Icons.add_circle_outline_outlined,
                        size: 40.0,
                        color: Styles.mainColor,
                      ),
              ),
              flex: 2,
              fit: FlexFit.tight,
            )
          ],
        ));
  }

  Widget customButton(
          {String labelText = 'Aceptar',
          Function function,
          bool isCancel = false}) =>
      GestureDetector(
        onTap: function,
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 5.0),
          width: MediaQuery.of(context).size.width * 0.25,
          height: 40,
          padding: EdgeInsets.symmetric(horizontal: 12),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
              color: isCancel ? Colors.red : Colors.blueAccent),
          child: Center(
            child: FittedBox(
              child: Text(
                labelText.toUpperCase(),
                style: TextStyle(
                    fontSize: 14,
                    color: Colors.white,
                    fontWeight: FontWeight.w400),
              ),
            ),
          ),
        ),
      );

  Widget accessCode() => TextFormField(
        validator: (val) => !val.contains('@') ? 'Usuario Incorrecto' : null,
        controller: accessCodeController,
        style: TextStyle(
            fontFamily: 'Montserrat', fontSize: 15.0, color: Colors.black),
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.person),
            labelText: 'Codigo de accesso al grupo',
            contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            border: OutlineInputBorder(
                borderSide: BorderSide(
                    color: Colors.blueAccent,
                    width: 1.0,
                    style: BorderStyle.solid),
                borderRadius: BorderRadius.circular(25.0))),
      );

  createAddGroupDialog(BuildContext context, Group group) async {
    showDialog(
        context: context,
        builder: (context) => Center(
                child: Container(
              margin: EdgeInsets.symmetric(horizontal: 40.0),
              width: MediaQuery.of(context).size.width * 0.8,
              height: 240,
              child: loadingGroup
                  ? CircularProgressIndicator()
                  : Material(
                      color: Colors.white.withOpacity(0.9),
                      elevation: 5.0,
                      shadowColor: Colors.black,
                      borderRadius: BorderRadius.circular(14.0),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 15.0, vertical: 15.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Flexible(
                                fit: FlexFit.loose,
                                flex: 7,
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text(
                                      'Desea unirse al grupo: '.toUpperCase(),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 18),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 8.0),
                                      child: Text(
                                        group.name.toUpperCase(),
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    SizedBox(height: 8.0),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 12.0, horizontal: 14.0),
                                      child: accessCode(),
                                    )
                                  ],
                                )),
                            Flexible(
                                fit: FlexFit.loose,
                                flex: 4,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Flexible(
                                      child: customButton(
                                          labelText: 'Unirme',
                                          function: () => joinGroup(group.id,
                                              accessCodeController.text)),
                                      flex: 5,
                                      fit: FlexFit.tight,
                                    ),
                                    Flexible(
                                      child: customButton(
                                          labelText: 'Cancelar',
                                          isCancel: true,
                                          function: () =>
                                              Navigator.pop(context)),
                                      flex: 5,
                                      fit: FlexFit.tight,
                                    )
                                  ],
                                )),
                          ],
                        ),
                      ),
                    ),
            )));
  }

  bool _verifyParticipants(Group group) {
    for (var internalGroup in myGroups) {
      if (group.id == internalGroup.id) {
        return true;
      }
    }
    return false;
  }
}
