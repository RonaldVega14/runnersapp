import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:runners_app/bloc/home_bloc.dart';
import 'package:runners_app/events/home_events.dart';
import 'package:runners_app/styles.dart';

class GroupCreation extends StatefulWidget {
  final HomeBloc homeBloc;
  final bool fromSuccess;

  const GroupCreation(
      {Key key, @required this.homeBloc, this.fromSuccess = true})
      : super(key: key);
  @override
  _GroupCreationState createState() => _GroupCreationState();
}

class _GroupCreationState extends State<GroupCreation> {
  TextEditingController nameController = new TextEditingController();
  TextEditingController descriptionController = new TextEditingController();
  TextEditingController accessCodeController = new TextEditingController();
  HomeBloc get homeBloc => widget.homeBloc;

  final formKey = new GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        centerTitle: true,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_outlined,
              color: Styles.mainColor,
            ),
            onPressed: () => homeBloc.add(ToHomePage())),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: ListView(
          children: [
            Text(
              "CREAR GRUPO",
              style: TextStyle(
                color: Styles.mainColor,
                fontSize: 26,
              ),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 30, right: 30, top: 50, bottom: 0),
              child: Form(
                  child: Column(children: <Widget>[
                TextFormField(
                  controller: nameController,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Tu grupo debe tener un nombre';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                      labelText: 'Nombre del grupo',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20.0))),
                ),
              ])),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 30, right: 30, top: 40, bottom: 0),
              child: Form(
                  child: Column(children: <Widget>[
                TextFormField(
                  controller: descriptionController,
                  decoration: InputDecoration(
                      labelText: 'Descripcion del grupo',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20.0))),
                ),
              ])),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 30, right: 30, top: 40, bottom: 0),
              child: Form(
                  key: formKey,
                  child: Column(children: <Widget>[
                    TextFormField(
                      controller: accessCodeController,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Tu grupo debe tener un código de acceso para nuevos integrantes';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          labelText: 'Código de acceso',
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20.0))),
                    ),
                  ])),
            ),
            SizedBox(height: 50),
            customButton(labelText: 'Guardar'),
            SizedBox(
              height: 50.0,
            )
          ],
        ),
      ),
    );
  }

  Widget customButton({String labelText = 'Aceptar'}) => GestureDetector(
        onTap: saveGroup,
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 80.0),
          width: MediaQuery.of(context).size.width * 0.45,
          height: 40,
          padding: EdgeInsets.symmetric(horizontal: 12),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6), color: Styles.mainColor),
          child: Center(
            child: FittedBox(
              child: Text(
                labelText.toUpperCase(),
                style: TextStyle(
                    fontSize: 14,
                    color: Colors.white,
                    fontWeight: FontWeight.w400),
              ),
            ),
          ),
        ),
      );

  void saveGroup() async {
    if (nameController.text.isNotEmpty && nameController.text != null) {
      if (descriptionController.text.isNotEmpty &&
          descriptionController.text != null) {
        if (accessCodeController.text.isNotEmpty &&
            accessCodeController.text != null) {
          homeBloc.add(CreateGroup(
            accessCode: accessCodeController.text,
            description: descriptionController.text,
            name: nameController.text,
          ));
        } else {
          Fluttertoast.showToast(
            msg: 'Agergar codigo de acceso al grupo por favor.',
            backgroundColor: Colors.redAccent,
          );
        }
      } else {
        Fluttertoast.showToast(
          msg: 'Agregar descripcion al grupo por favor.',
          backgroundColor: Colors.redAccent,
        );
      }
    } else {
      Fluttertoast.showToast(
        msg: 'Agregar nombre al grupo por favor.',
        backgroundColor: Colors.redAccent,
      );
    }
  }
}
