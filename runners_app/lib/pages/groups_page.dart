import 'package:flutter/material.dart';
import 'package:runners_app/bloc/home_bloc.dart';
import 'package:runners_app/events/home_events.dart';
import 'package:runners_app/models/group_model.dart';
import 'package:runners_app/models/user_model.dart';
import 'package:runners_app/pages/group_details_page.dart';
import 'package:runners_app/styles.dart';

class Groups extends StatefulWidget {
  final User user;
  final HomeBloc homeBloc;

  const Groups({Key key, @required this.user, @required this.homeBloc})
      : super(key: key);
  @override
  _GroupsState createState() => new _GroupsState();
}

class _GroupsState extends State<Groups> {
  List<Group> get myGroups => widget.user.groups;
  User get user => widget.user;
  HomeBloc get homeBloc => widget.homeBloc;

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        centerTitle: true,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_outlined,
              color: Styles.mainColor,
            ),
            onPressed: () => homeBloc.add(ToHomePage())),
      ),
      body: SafeArea(
        child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: user.groups.length > 0
                ? _buildMyGroupsList()
                : Center(
                    child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        padding: EdgeInsets.fromLTRB(80.0, 0.0, 80.0, 30.0),
                        child: Center(
                          child: SizedBox(
                            height: 120.0,
                            child: Image.asset(
                              "assets/runner_logo.png",
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
                      ),
                      Text(
                        'Todavia no pertenece a ningun grupo...',
                        style: TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 40.0),
                      GestureDetector(
                        onTap: () => homeBloc.add(ToHomePage()),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.40,
                          height: 40,
                          padding: EdgeInsets.symmetric(horizontal: 12),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              color: Styles.mainColor),
                          child: Center(
                            child: FittedBox(
                              child: Text(
                                'Regresar'.toUpperCase(),
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 50.0),
                    ],
                  ))),
      ),
    );
  }

  Widget _buildMyGroupsList() {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Flexible(
              flex: 2,
              fit: FlexFit.loose,
              child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Text(
                    'Mis Grupos'.toUpperCase(),
                    style: TextStyle(
                      color: Styles.mainColor,
                      fontSize: 26,
                    ),
                    textAlign: TextAlign.center,
                  ))),
          SizedBox(
            height: 15.0,
          ),
          Flexible(
            flex: 15,
            fit: FlexFit.loose,
            child: ListView.builder(
              itemBuilder: (context, index) => _groupContainer(myGroups[index]),
              itemCount: myGroups.length,
            ),
          )
        ],
      ),
    );
  }

  Widget _groupContainer(Group group) => Container(
      margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
      padding: EdgeInsets.symmetric(vertical: 3.0, horizontal: 15.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12.0),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            offset: Offset(0.0, 1.0), //(x,y)
            blurRadius: 6.0,
          ),
        ],
      ),
      height: 80,
      width: MediaQuery.of(context).size.width,
      child: Row(
        children: [
          Flexible(
            flex: 8,
            fit: FlexFit.tight,
            child: Column(
              children: [
                Flexible(
                    flex: 2,
                    fit: FlexFit.tight,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'Nombre del grupo: ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16.0),
                        ),
                        Text(
                          group.name,
                          style: TextStyle(fontSize: 16.0),
                        )
                      ],
                    )),
                Flexible(
                    flex: 2,
                    fit: FlexFit.tight,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'Numero de miembros: ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16.0),
                        ),
                        Text(
                          group.members != null
                              ? group.members.length.toString()
                              : '0',
                          style: TextStyle(fontSize: 16.0),
                        )
                      ],
                    ))
              ],
            ),
          ),
          Flexible(
            child: GestureDetector(
              onTap: () => homeBloc.add(ToGroupDetailsPage(group: group)),
              child: Icon(
                Icons.arrow_forward_ios_outlined,
                size: 32.0,
                color: Styles.mainColor,
              ),
            ),
            flex: 2,
            fit: FlexFit.tight,
          )
        ],
      ));
}
