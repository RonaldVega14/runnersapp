import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:runners_app/bloc/home_bloc.dart';
import 'package:runners_app/events/home_events.dart';
import 'package:runners_app/pages/add_route_page.dart';
import 'package:runners_app/repositories/user_repository.dart';
import 'package:runners_app/styles.dart';

class DashboardPage extends StatefulWidget {
  final HomeBloc homeBloc;
  const DashboardPage({Key key, @required this.homeBloc}) : super(key: key);
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  HomeBloc get homeBloc => widget.homeBloc;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        actions: [
          IconButton(
              icon: Icon(Icons.logout, color: Colors.redAccent),
              onPressed: _signOut)
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(top: 5.0),
              child: Center(
                child: SizedBox(
                  height: 100.0,
                  width: 300,
                  child: Image.asset(
                    "assets/runner_logo.png",
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 50.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GestureDetector(
                    onTap: () => homeBloc.add(ToCreateGroupPage()),
                    child: homeContainer(
                        context, Icons.group_add_outlined, 'Crear grupo')),
                GestureDetector(
                    onTap: () => homeBloc.add(ToMyGroupsPage()),
                    child: homeContainer(
                        context, Icons.run_circle_outlined, 'Ver mis grupos')),
              ],
            ),
            SizedBox(
              height: 35.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GestureDetector(
                    onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AddRoutePage(
                                  backToHome: true,
                                  title: 'Crear una ruta personal',
                                  homeBloc: homeBloc,
                                ))),
                    child: homeContainer(context, Icons.add_box_outlined,
                        'Crear una ruta personal')),
                GestureDetector(
                    onTap: () => homeBloc.add(ToAllGroupsPage()),
                    child: homeContainer(context, Icons.workspaces_outline,
                        'Ver todos los grupos')),
              ],
            ),
            SizedBox(
              height: 55.0,
            ),
            Center(
              child: GestureDetector(
                onTap: _signOut,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 5.0),
                  width: MediaQuery.of(context).size.width * 0.45,
                  height: 40,
                  padding: EdgeInsets.symmetric(horizontal: 12),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Styles.mainColor),
                  child: Center(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Flexible(
                          child: Icon(
                            Icons.logout,
                            color: Colors.white,
                          ),
                          fit: FlexFit.loose,
                          flex: 2),
                      Flexible(
                        child: Text(
                          'Cerrar Sesion'.toUpperCase(),
                          style: TextStyle(
                              fontSize: 14,
                              color: Colors.white,
                              fontWeight: FontWeight.w400),
                        ),
                        fit: FlexFit.loose,
                        flex: 8,
                      )
                    ],
                  )),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget homeContainer(BuildContext context, IconData icon, String title) {
    return Container(
        height: MediaQuery.of(context).size.height * 0.18,
        width: MediaQuery.of(context).size.width * 0.35,
        padding: EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12.0),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              offset: Offset(0.0, 1.0), //(x,y)
              blurRadius: 6.0,
            ),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Icon(
              icon,
              size: 48.0,
            ),
            Text(
              title,
              style: TextStyle(fontSize: 16.0, fontFamily: 'Montserrat'),
              textAlign: TextAlign.center,
            )
          ],
        ));
  }

  void _signOut() async {
    UserRepository userRepository = UserRepository();
    userRepository.deleteToken();
    Fluttertoast.showToast(
        msg: 'Adios, te esperamos de vuelta pronto',
        backgroundColor: Colors.blueAccent);
    Navigator.of(context).popUntil((route) => route.isFirst);
  }
}
