import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:runners_app/models/group_model.dart';
import 'package:runners_app/models/user_model.dart';

abstract class HomeEvent extends Equatable {}

class ToHomePage extends HomeEvent {
  final bool needsToRefresh;

  ToHomePage({this.needsToRefresh = false});
  @override
  String toString() => 'ToHomePage';
}

class ToCreateGroupPage extends HomeEvent {
  @override
  String toString() => 'ToCreateGroup';
}

class CreateGroup extends HomeEvent {
  final String name, description, accessCode;

  CreateGroup(
      {@required this.name,
      @required this.description,
      @required this.accessCode});
  @override
  String toString() => 'SaveGroup';
}

class ToMyGroupsPage extends HomeEvent {
  @override
  String toString() => 'ToMyGroupsPage';
}

class ToGroupDetailsPage extends HomeEvent {
  final Group group;

  ToGroupDetailsPage({@required this.group});
  @override
  String toString() => 'ToGroupDetailsPage';
}

class ToAllGroupsPage extends HomeEvent {
  @override
  String toString() => 'ToAllGroupsPage';
}

class JoinGroup extends HomeEvent {
  final String accessCode, groupID;
  final List<Group> groups;

  JoinGroup(
      {@required this.accessCode,
      @required this.groupID,
      @required this.groups});
  @override
  String toString() => 'JoinGroup';
}

class ToCreateRoutePage extends HomeEvent {
  final Group group;
  final String title;

  ToCreateRoutePage({@required this.group, @required this.title});
  @override
  String toString() => 'ToCreateRoutePage';
}

class AddRouteToGroup extends HomeEvent {
  final String name, startLat, startLng, endLat, endLng;
  final Group group;

  AddRouteToGroup(
      {@required this.name,
      @required this.group,
      @required this.startLat,
      @required this.startLng,
      @required this.endLat,
      @required this.endLng});
  @override
  String toString() => 'AddRouteToGroup';
}
