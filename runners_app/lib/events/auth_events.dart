import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class AuthEvent extends Equatable {
  AuthEvent([List props = const []]) : super(props);
}

class AppStarted extends AuthEvent {
  @override
  String toString() => 'AppStarted';
}

class LoggedIn extends AuthEvent {
  final String username;
  final String password;

  LoggedIn({@required this.username, @required this.password})
      : super([username]);

  @override
  String toString() => 'LoggedIn';
}

class LoggedOut extends AuthEvent {
  @override
  String toString() => 'LoggedOut';
}

class BackToLogin extends AuthEvent {
  @override
  String toString() => 'BackToLogin';
}

class RegisterUser extends AuthEvent {
  final String nombres, apellidos, correo, usuario, contra;

  RegisterUser(
      {@required this.nombres,
      @required this.apellidos,
      @required this.correo,
      @required this.usuario,
      @required this.contra});
  @override
  String toString() => 'RegisterUser';
}
