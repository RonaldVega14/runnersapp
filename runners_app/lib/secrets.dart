import 'package:flutter/material.dart';

class Secrets {
  // Add your Google Maps API Key here
  static const API_KEY = 'AIzaSyDHxH3teeQ_A0mbNpDaxZKPjiNlF3byRIw';

  Widget customButton({String labelText = 'Aceptar'}) => GestureDetector(
        onTap: () => null,
        child: Container(
          constraints: BoxConstraints(minWidth: 100, maxWidth: 135),
          height: 40,
          padding: EdgeInsets.symmetric(horizontal: 12),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
              color: Color.fromRGBO(64, 186, 213, 1)),
          child: Center(
            child: FittedBox(
              child: Text(
                labelText.toUpperCase(),
                style: TextStyle(
                    fontSize: 14,
                    color: Colors.white,
                    fontWeight: FontWeight.w400),
              ),
            ),
          ),
        ),
      );
}
