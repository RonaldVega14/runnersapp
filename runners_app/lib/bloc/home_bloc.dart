import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:runners_app/events/home_events.dart';
import 'package:runners_app/models/group_model.dart';
import 'package:runners_app/models/user_model.dart';
import 'package:runners_app/repositories/group_repository.dart';
import 'package:runners_app/repositories/network_helper.dart';
import 'package:runners_app/repositories/user_repository.dart';
import 'package:runners_app/states/home_states.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final User user;
  final GroupRepository groupRepository = GroupRepository();
  final UserRepository userRepository = UserRepository();
  final NetworkHelper networkHelper = NetworkHelper();
  HomeBloc(HomeState initialState, {@required this.user}) : super(initialState);

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    //Evento cuando el bloc inicia y dirige a dashboard page
    if (event is ToHomePage) {
      Fluttertoast.showToast(
          msg: 'Cargando Informacion del perfil...',
          backgroundColor: Colors.blueAccent);
      final response = await groupRepository.findGroupsByUsername();
      if (response != null) {
        //Si la respuesta no da ningun error se redirecciona a HomePage
        user.groups = response;
        yield OnHomePage();
      } else {
        //Si la peticion da error se muestra un mensaje de error.
        Fluttertoast.showToast(
            msg:
                'Error al obtener la lista de tus grupos, intentelo mas tarde.',
            backgroundColor: Colors.redAccent);
        yield OnHomePage();
      }
      //Este evento se llama para redireccionar a la pantalla de crear grupo.
    } else if (event is ToCreateGroupPage) {
      yield OnCreateGroupPage();
      //Este evento se llama cuando se presiona el boton para crear un nuevo grupo.
    } else if (event is CreateGroup) {
      final response = await groupRepository.createGroup(
          name: event.name,
          description: event.description,
          accessCode: event.accessCode);
      if (response) {
        Fluttertoast.showToast(
            msg: 'Grupo creado con exito', backgroundColor: Colors.blueAccent);
        yield OnHomePage();
      } else {
        Fluttertoast.showToast(
          msg: 'Error al crear grupo, intentelo de nuevo.',
          backgroundColor: Colors.redAccent,
        );
        yield OnCreateGroupPage();
      }
    } else if (event is ToMyGroupsPage) {
      Fluttertoast.showToast(
          msg: 'Cargando grupos del perfil...',
          backgroundColor: Colors.blueAccent);
      final response = await groupRepository.findGroupsByUsername();
      if (response != null) {
        if (user != null) {
          user.groups = response;
          yield OnMyGroupsPage(user: user);
        } else {
          Fluttertoast.showToast(
              msg:
                  'Ocurrio un error en el codigo, porfavor reporte el error con los desarrolladores',
              backgroundColor: Colors.redAccent);
          yield OnHomePage();
        }
      } else {
        Fluttertoast.showToast(
            msg: 'Error al obtener la lista de tus grupos, intentelo mas tarde',
            backgroundColor: Colors.redAccent);
        yield OnHomePage();
      }
    } else if (event is ToAllGroupsPage) {
      Fluttertoast.showToast(
          msg: 'Cargando todos los grupos...',
          backgroundColor: Colors.blueAccent);
      final response = await groupRepository.getAllGroups();
      if (response != null) {
        yield OnAllGroupsPage(groups: response);
      } else {
        Fluttertoast.showToast(
            msg: 'Error al obtner los grupos, intentelo de nuevo.',
            backgroundColor: Colors.redAccent);
      }
    } else if (event is JoinGroup) {
      Fluttertoast.showToast(
          msg: 'Agregando a grupo...', backgroundColor: Colors.blueAccent);
      Group newGroup = await groupRepository.joinGroup(
          accessCode: event.accessCode, groupID: event.groupID);
      if (newGroup != null) {
        if (newGroup.id == '403') {
          Fluttertoast.showToast(
              msg: newGroup.description,
              backgroundColor: Colors.redAccent,
              toastLength: Toast.LENGTH_LONG);
        } else {
          user.groups.add(newGroup);
          Fluttertoast.showToast(
              msg: 'Agregado a ${newGroup.name} con exito',
              backgroundColor: Colors.blueAccent);
        }
      } else {
        Fluttertoast.showToast(
            msg: 'Error de conexion, intentelo de nuevo',
            backgroundColor: Colors.redAccent);
      }
      yield OnAllGroupsPage(groups: event.groups);
    } else if (event is ToGroupDetailsPage) {
      Fluttertoast.showToast(
          msg: 'Cargando Informacion del grupo...',
          backgroundColor: Colors.blueAccent);
      final response =
          await groupRepository.getGroupRoutes(groupID: event.group.id);
      if (response != null) {
        event.group.routes = response;
        if (_updateUserGroup(event.group)) {
          yield OnGroupDetailsPage(group: event.group);
        } else {
          Fluttertoast.showToast(
              msg: 'Error al actualizar rutas del grupo, intentelo de nuevo',
              backgroundColor: Colors.redAccent);
          yield OnMyGroupsPage(user: user);
        }
      } else {
        Fluttertoast.showToast(
            msg: 'Error al cargar las rutas del grupo, intentelo de nuevo',
            backgroundColor: Colors.redAccent);
        yield OnMyGroupsPage(user: user);
      }
    } else if (event is JoinGroup) {
      Fluttertoast.showToast(
          msg: 'Agregando a grupo...', backgroundColor: Colors.blueAccent);
      Group newGroup = await groupRepository.joinGroup(
          accessCode: event.accessCode, groupID: event.groupID);
      if (newGroup != null) {
        if (newGroup.id == '403') {
          Fluttertoast.showToast(
              msg: newGroup.description,
              backgroundColor: Colors.redAccent,
              toastLength: Toast.LENGTH_LONG);
        } else {
          user.groups.add(newGroup);
          Fluttertoast.showToast(
              msg: 'Agregado a ${newGroup.name} con exito',
              backgroundColor: Colors.blueAccent);
        }
      } else {
        Fluttertoast.showToast(
            msg: 'Error de conexion, intentelo de nuevo',
            backgroundColor: Colors.redAccent);
      }
      yield OnAllGroupsPage(groups: event.groups);
    } else if (event is AddRouteToGroup) {
      Fluttertoast.showToast(
          msg: 'Agregando ruta al grupo...',
          backgroundColor: Colors.blueAccent);
      final response = await groupRepository.addRouteToGroup(
          groupId: event.group.id,
          startingPoint: event.startLat + ', ' + event.startLng,
          finishingPoint: event.endLat + ', ' + event.endLng,
          routeName: event.name);
      if (response != null) {
        event.group.routes.add(response);
        Fluttertoast.showToast(
            msg: 'Ruta agregada a con exito!',
            backgroundColor: Colors.blueAccent);
        if (_updateUserGroup(event.group)) {
          yield OnGroupDetailsPage(group: event.group);
        }
      } else {
        Fluttertoast.showToast(
            msg: 'Error al guardar ruta, intentelo de nuevo.',
            backgroundColor: Colors.redAccent);
        //TODO: Add state to go to map page
      }
    } else if (event is ToAllGroupsPage) {
      yield LoadingScreen(message: 'Cargando todos los grupos...');
      final response = await groupRepository.getAllGroups();
      if (response != null) {
        yield OnAllGroupsPage(groups: response);
      } else {
        Fluttertoast.showToast(
            msg: 'Error al obtner los grupos, intentelo de nuevo.',
            backgroundColor: Colors.redAccent);
      }
    } else if (event is JoinGroup) {
      Fluttertoast.showToast(
          msg: 'Agregando a grupo...', backgroundColor: Colors.blueAccent);
      Group newGroup = await groupRepository.joinGroup(
          accessCode: event.accessCode, groupID: event.groupID);
      if (newGroup != null) {
        if (newGroup.id == '403') {
          Fluttertoast.showToast(
              msg: newGroup.description,
              backgroundColor: Colors.redAccent,
              toastLength: Toast.LENGTH_LONG);
        } else {
          user.groups.add(newGroup);
          Fluttertoast.showToast(
              msg: 'Agregado a ${newGroup.name} con exito',
              backgroundColor: Colors.blueAccent);
        }
      } else {
        Fluttertoast.showToast(
            msg: 'Error de conexion, intentelo de nuevo',
            backgroundColor: Colors.redAccent);
      }
      yield OnAllGroupsPage(groups: event.groups);
    } else if (event is ToCreateRoutePage) {
      Fluttertoast.showToast(
          msg: 'Cargando mapa...', backgroundColor: Colors.blueAccent);
      yield OnCreateRoutePage(title: event.title, group: event.group);
    } else {
      throw UnimplementedError();
    }
  }

  bool _updateUserGroup(Group group) {
    for (var i = 0; i < user.groups.length; i++) {
      if (user.groups[i].id == group.id) {
        user.groups[i] = group;
        return true;
      }
    }
    return false;
  }
}
