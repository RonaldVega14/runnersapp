import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:runners_app/events/auth_events.dart';
import 'package:runners_app/repositories/user_repository.dart';
import 'package:runners_app/states/auth_states.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final UserRepository userRepository;
  AuthBloc(AuthState initialState, {@required this.userRepository})
      : super(initialState);

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is LoggedIn) {
      yield AuthenticationLoading();
      final userModel = await userRepository.authenticate(
          username: event.username, password: event.password);

      if (userModel != null) {
        Fluttertoast.showToast(
            msg: 'Bienvenido/a ${userModel.nombre}',
            backgroundColor: Colors.blueAccent);
        yield AuthenticationAuthenticated(userModel);
      } else {
        Fluttertoast.showToast(
            msg: 'Error de autenticacion, intentelo de nuevo',
            backgroundColor: Colors.redAccent);
        yield AuthenticationUnauthenticated();
      }
    } else if (event is AppStarted) {
      yield AuthenticationUnauthenticated();
    } else if (event is BackToLogin) {
      yield AuthenticationUnauthenticated();
    } else if (event is RegisterUser) {
      Fluttertoast.showToast(
          msg: 'Registrando usuario...', backgroundColor: Colors.blueAccent);
      final userModel = await userRepository.registerUser(
          nombre: event.nombres,
          apellido: event.apellidos,
          email: event.correo,
          username: event.usuario,
          password: event.contra);

      if (userModel != null) {
        Fluttertoast.showToast(
            msg: 'Usuario creado con exito',
            backgroundColor: Colors.blueAccent);
        yield AuthenticationUnauthenticated();
      } else {
        Fluttertoast.showToast(
            msg: 'Ocurrio un error al crear el usuario, intentelo de nuevo.',
            backgroundColor: Colors.redAccent);
      }
    } else {
      print('Unimplementhed State');
      throw UnimplementedError();
    }
  }
}
