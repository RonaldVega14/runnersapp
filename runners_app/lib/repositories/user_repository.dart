import 'dart:convert';
import 'package:runners_app/pages/dashboard_page.dart';
import 'package:runners_app/repositories/network_util.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/user_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

const String TAG = 'UserRepository';

class UserRepository {
  User user = new User();
  SharedPreferences _prefs;

  Future<User> authenticate(
      {@required String username, @required String password}) async {
    try {
      //Creando la url que se va a utilizar
      String url = NetworkUtils.path + 'auth/signin';
      //realizando peticion al servidor
      final response = await http
          .post(url, body: {"identifier": username, "password": password});
      if (response.statusCode == 200 || response.statusCode == 201) {
        //Mostrando mensaje de exito y asignamos la respuesta al usuario creado
        user = User.fromJson(json.decode(response.body));
        _prefs = await SharedPreferences.getInstance();
        await _prefs.setString('username', user.username);
        await _prefs.setString('token', user.token);
        await _prefs.setString('id', user.id);
        return user;
      } else {
        return null;
      }
    } catch (e) {
      print('ERROR: $TAG: authenticate: ' + e.toString());
      return null;
    }
  }

  Future<User> registerUser(
      {@required String nombre,
      @required String apellido,
      @required String email,
      @required String username,
      @required String password}) async {
    try {
      String url = NetworkUtils.path + 'auth/signup';
      final response = await http.post(url, body: {
        "firstNames": nombre,
        "lastNames": apellido,
        "email": email,
        "username": username,
        "password": password,
      });
      if (response.statusCode == 201 || response.statusCode == 200) {
        //Mostrando mensaje de exito y asignamos la respuesta al usuario creado
        User user = User.fromJson(json.decode(response.body));
        _prefs = await SharedPreferences.getInstance();
        await _prefs.setString('username', user.username);

        return user;
      }
    } catch (e) {
      print('ERROR: $TAG: registerUser: ' + e.toString());
    }
    return null;
  }

  void deleteToken() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.clear();
  }

  void changeIsFirst(bool value) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.setBool('isFirst', value);
  }
}
