import 'dart:convert';
import 'package:runners_app/models/group_model.dart';
import 'package:runners_app/models/route_model.dart';
import 'package:runners_app/repositories/network_util.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/user_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

const String TAG = 'GroupRepository';

class GroupRepository {
  Future<bool> createGroup(
      {@required String name,
      @required String description,
      @required String accessCode}) async {
    try {
      String url = NetworkUtils.path + 'groups/create';
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      String token = _prefs.getString('token');
      final response = await http.post(url, headers: {
        'Authorization': 'Bearer $token',
      }, body: {
        "name": name,
        "description": description,
        "accessCode": accessCode
      });

      if (response.statusCode == 201) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print('ERROR: $TAG: createGroup: ' + e.toString());
      return false;
    }
  }

  Future<List<Group>> getAllGroups() async {
    try {
      String url = NetworkUtils.path + 'groups/';
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      String token = _prefs.getString('token');
      final response = await http.get(
        url,
        headers: {
          'Authorization': 'Bearer $token',
        },
      );

      if (response.statusCode == 200) {
        return Group.fromJsonArray(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      print('ERROR: $TAG: getAllGroups: ' + e.toString());
      return null;
    }
  }

  Future<List<Group>> findGroupsByUsername() async {
    try {
      String url = NetworkUtils.path + 'users/groups';
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      String token = _prefs.getString('token');
      print(token);
      final response = await http.get(
        url,
        headers: {
          'Authorization': 'Bearer $token',
        },
      );
      if (response.statusCode == 201 || response.statusCode == 200) {
        List<Group> myGroups = Group.fromJsonObject(json.decode(response.body));

        return myGroups;
      }
    } catch (e) {
      print('ERROR: $TAG: findGroupsByUsername: ' + e.toString());
    }
    return null;
  }

  Future<Group> joinGroup(
      {@required String accessCode, @required String groupID}) async {
    try {
      String url = NetworkUtils.path + 'users/member';
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      String token = _prefs.getString('token');
      String id = _prefs.getString('id');
      final response = await http.patch(url, headers: {
        'Authorization': 'Bearer $token',
      }, body: {
        "userId": id,
        "groupId": groupID,
        "accessCode": accessCode
      });
      if (response.statusCode == 201 || response.statusCode == 200) {
        Group group = Group.fromGroupJoined(json.decode(response.body));
        return group;
      } else if (response.statusCode == 403) {
        Group group =
            Group(description: 'Codigo de accesso equivocado', id: '403');
        return group;
      }
    } catch (e) {
      print('ERROR: $TAG: joinGroup: ' + e.toString());
      return null;
    }
    return null;
  }

  Future<List<MapRoute>> getGroupRoutes({@required String groupID}) async {
    try {
      String url = NetworkUtils.path + 'groups/routes/$groupID';
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      String token = _prefs.getString('token');

      final response = await http.get(url, headers: {
        'Authorization': 'Bearer $token',
      });
      if (response.statusCode == 201 || response.statusCode == 200) {
        List<MapRoute> groupRoutes =
            MapRoute.fromJsonArray(json.decode(response.body));
        return groupRoutes;
      }
    } catch (e) {
      print('ERROR: $TAG: getGroupRoutes: ' + e.toString());
      return null;
    }
    return null;
  }

  Future<MapRoute> addRouteToGroup(
      {@required String groupId,
      @required String startingPoint,
      @required String finishingPoint,
      @required String routeName}) async {
    try {
      String url = NetworkUtils.path + 'routes/create';
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      String token = _prefs.getString('token');
      final response = await http.post(url, headers: {
        'Authorization': 'Bearer $token',
      }, body: {
        "name": routeName,
        "startingPoint": startingPoint,
        "finishingPoint": finishingPoint,
        "groupId": groupId
      });
      if (response.statusCode == 201 || response.statusCode == 200) {
        MapRoute route = MapRoute.fromJson(json.decode(response.body));
        return route;
      }
    } catch (e) {
      print('ERROR: $TAG: addRouteToGroup: ' + e.toString());
      return null;
    }
    return null;
  }
}
