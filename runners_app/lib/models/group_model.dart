import 'package:runners_app/models/route_model.dart';
import 'package:runners_app/models/user_model.dart';

class Group {
  String name;
  String description;
  String accessCode;
  List<String> admins;
  List<String> members;
  List<MapRoute> routes;
  String photo;
  String id;

  Group(
      {this.name,
      this.description,
      this.accessCode,
      this.admins,
      this.members,
      this.id,
      this.routes,
      this.photo});

  factory Group.fromJson(Map<String, dynamic> json) {
    return Group(
        name: json['name'] ?? '',
        description: json['description'] ?? '',
        accessCode: json['accessCode'] ?? '',
        id: json['_id'],
        members: assignIds(json['members']),
        admins: assignIds(json['admins']),
        routes: []);
  }

  static assignIds(List<dynamic> json) {
    List<String> ids = [];
    for (var i = 0; i < json.length; i++) {
      ids.add(json[i].toString());
    }
    return ids;
  }

  static List<Group> fromJsonArray(List<dynamic> parsedJson) {
    List<Group> allGroups = [];
    if (parsedJson != null && parsedJson.toString() != 'null') {
      for (var i = 0; i < parsedJson.length; i++) {
        Group group = Group.fromJson(parsedJson[i]);
        allGroups.add(group);
      }
    }
    return allGroups;
  }

  static List<Group> fromJsonObject(Map<String, dynamic> json) {
    List<Group> allGroups = [];
    List<dynamic> parsedJson = json['groups'];
    if (parsedJson != null && parsedJson.toString() != 'null') {
      for (var i = 0; i < parsedJson.length; i++) {
        Group group = Group.fromJson(parsedJson[i]);
        allGroups.add(group);
      }
    }
    return allGroups;
  }

  factory Group.fromGroupJoined(Map<String, dynamic> json) {
    Map<String, dynamic> parsedJson = json['group'];
    if (parsedJson != null && parsedJson.toString() != 'null') {
      Group group = Group.fromJson(parsedJson);
      return group;
    }
    return null;
  }
}
