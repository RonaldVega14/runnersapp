import 'package:runners_app/models/group_model.dart';

class User {
  String nombre;
  String apellido;
  String email;
  String username;
  String password;
  String id;
  String token;
  List<Group> groups;

  User(
      {this.nombre,
      this.apellido,
      this.email,
      this.username,
      this.password,
      this.id,
      this.token,
      this.groups});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        nombre: json['firstNames'].toString() ?? '',
        apellido: json['lastNames'].toString() ?? '',
        email: json['email'].toString() ?? '',
        password: json['password'].toString() ?? '',
        username: json['username'].toString() ?? '',
        id: json['_id'].toString(),
        token: json['token'].toString() ?? '',
        groups: []);
  }
}
