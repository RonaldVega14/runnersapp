import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:date_format/date_format.dart';

class MapRoute {
  String id;
  String name;
  LatLng startingPoint;
  LatLng finishingPoint;
  String groupId;
  String createdAt;

  MapRoute(
      {this.createdAt,
      this.finishingPoint,
      this.groupId,
      this.id,
      this.name,
      this.startingPoint});

  factory MapRoute.fromJson(Map<String, dynamic> json) {
    LatLng fromStringToLatLng(String coordenate) {
      if (coordenate.isEmpty) {
        return null;
      } else {
        String latitude =
            coordenate.split(',').elementAt(0).replaceAll(' ', '');
        String longitude =
            coordenate.split(',').elementAt(1).replaceAll(' ', '');

        return LatLng(double.parse(latitude), double.parse(longitude));
      }
    }

    String convertDateFromString(String strDate) {
      DateTime todayDate = DateTime.parse(strDate);
      return (formatDate(todayDate, [dd, '/', mm, '/', yyyy]));
    }

    return MapRoute(
      name: json['name'] ?? '',
      id: json['_id'] ?? '',
      startingPoint: fromStringToLatLng(json['startingPoint']) ?? '',
      finishingPoint: fromStringToLatLng(json['finishingPoint']) ?? '',
      groupId: json['groupId'] ?? '',
      createdAt: convertDateFromString(json['createdAt']) ?? '',
    );
  }

  static List<MapRoute> fromJsonArray(Map<String, dynamic> json) {
    List<MapRoute> routes = [];
    if (json.isNotEmpty && json != null) {
      List<dynamic> parsedRoutes = json['routes'];
      for (var i = 0; i < parsedRoutes.length; i++) {
        MapRoute newRoute = MapRoute.fromJson(parsedRoutes[i]);
        routes.add(newRoute);
        print('route name: ' + newRoute.name);
      }
      return routes;
    }
    return null;
  }
}
