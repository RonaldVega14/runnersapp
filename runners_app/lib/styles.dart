import 'package:flutter/material.dart';

class Styles {
  static Color get mainColor => Color.fromRGBO(160, 16, 39, 1);
  static Color get secondaryColor => Color.fromRGBO(116, 64, 72, 1);
  static Color get darkerColor => Color.fromRGBO(68, 60, 67, 1);
}
