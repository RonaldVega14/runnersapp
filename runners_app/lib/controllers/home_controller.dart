import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:runners_app/bloc/home_bloc.dart';
import 'package:runners_app/events/home_events.dart';
import 'package:runners_app/models/user_model.dart';
import 'package:runners_app/pages/add_route_page.dart';
import 'package:runners_app/pages/allGroups_page.dart';
import 'package:runners_app/pages/dashboard_page.dart';
import 'package:runners_app/pages/group_creation_page.dart';
import 'package:runners_app/pages/group_details_page.dart';
import 'package:runners_app/pages/groups_page.dart';
import 'package:runners_app/pages/splash_page.dart';
import 'package:runners_app/states/home_states.dart';

class HomeController extends StatefulWidget {
  final User user;

  const HomeController({Key key, @required this.user}) : super(key: key);
  @override
  _HomeControllerState createState() => _HomeControllerState();
}

class _HomeControllerState extends State<HomeController> {
  HomeBloc _homeBloc;

  @override
  void initState() {
    _homeBloc = HomeBloc(OnHomePage(), user: widget.user);
    _homeBloc.add(ToHomePage(needsToRefresh: true));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (BuildContext context) => _homeBloc,
        child: BlocListener<HomeBloc, HomeState>(
          cubit: _homeBloc,
          listener: (BuildContext context, HomeState state) {},
          child: BlocBuilder(
            cubit: _homeBloc,
            builder: (BuildContext context, state) {
              return AnimatedSwitcher(
                duration: Duration(milliseconds: 250),
                switchOutCurve: Threshold(0.05),
                transitionBuilder: (Widget child, Animation<double> animation) {
                  return SlideTransition(
                    position: Tween<Offset>(
                      begin: const Offset(0.25, 0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: child,
                  );
                },
                child: _buildPage(context, state),
              );
            },
          ),
        ));
  }

  Widget _buildPage(BuildContext context, HomeState state) {
    if (state is OnHomePage) {
      return DashboardPage(
        homeBloc: _homeBloc,
      );
    } else if (state is OnCreateGroupPage) {
      return GroupCreation(homeBloc: _homeBloc);
    } else if (state is OnMyGroupsPage) {
      return Groups(
        user: state.user,
        homeBloc: _homeBloc,
      );
    } else if (state is OnGroupDetailsPage) {
      return GroupDetailsPage(
        group: state.group,
        homeBloc: _homeBloc,
      );
    } else if (state is OnAllGroupsPage) {
      return AllGroupsPage(
        homeBloc: _homeBloc,
        groups: state.groups,
      );
    } else if (state is LoadingScreen) {
      return SplashPage();
    } else if (state is OnCreateRoutePage) {
      return AddRoutePage(
        homeBloc: _homeBloc,
        title: state.title,
        group: state.group,
      );
    } else {
      return SplashPage();
    }
  }
}
