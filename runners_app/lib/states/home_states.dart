import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:runners_app/models/group_model.dart';
import 'package:runners_app/models/user_model.dart';

abstract class HomeState extends Equatable {}

class OnHomePage extends HomeState {
  @override
  String toString() => 'OnHomePage';
}

class OnCreateGroupPage extends HomeState {
  @override
  String toString() => 'OnCreateGroupPage';
}

class OnMyGroupsPage extends HomeState {
  final User user;

  OnMyGroupsPage({@required this.user});
  @override
  String toString() => 'OnCreateBusinessPage';
}

class OnGroupDetailsPage extends HomeState {
  final Group group;

  OnGroupDetailsPage({@required this.group});
  @override
  String toString() => 'OnCreateBusinessPage';
}

class OnAllGroupsPage extends HomeState {
  final List<Group> groups;

  OnAllGroupsPage({@required this.groups});
  @override
  String toString() => 'OnAllGroupsPage';
}

class LoadingScreen extends HomeState {
  final String message;

  LoadingScreen({@required this.message});
  @override
  String toString() => 'LoadingScreen';
}

class OnCreateRoutePage extends HomeState {
  final String title;
  final Group group;

  OnCreateRoutePage({@required this.title, @required this.group});
  @override
  String toString() => 'OnCreateRoutePage';
}
