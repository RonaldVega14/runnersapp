import 'package:equatable/equatable.dart';
import 'package:runners_app/models/user_model.dart';

abstract class AuthState extends Equatable {}

class AuthenticationUninitialized extends AuthState {
  @override
  String toString() => 'AuthenticationUninitialized';
}

class AuthenticationAuthenticated extends AuthState {
  final User user;

  AuthenticationAuthenticated(this.user);
  @override
  String toString() => 'AuthenticationAuthenticated';
}

class AuthenticationUnauthenticated extends AuthState {
  @override
  String toString() => 'AuthenticationUnauthenticated';
}

class AuthenticationLoading extends AuthState {
  @override
  String toString() => 'AuthenticationLoading';
}

class RegistrationSubmitted extends AuthState {
  @override
  String toString() => 'RegistrationFormSubmitted';
}
